# WebService

Sử dụng pip để cài đặt các công cụ môi trường ảo: pip install virtualenv
Tạo môi trường ảo để chạy Python của dự án: virtualenv env
Để có thể chạy các đoạn script trong power shell cần setting lại policy thông qua lệnh:            set-executionpolicy unrestricted
Kích hoạt môi trường: env/Scripts/activate
Trong môi trường ảo:
	+ Để cài đặt các phụ thuộc của dự án bằng tập tin requirements.txt: pip install -r requirements.txt
	(Nếu quá trình chạy các extension trong requirements bị lỗi: Microsoft Visual C++ 9.0 is required
	Nếu xài python version 2.7 thì mọi người cài thêm Microsoft Visual C++ Compiler, link download: https://www.microsoft.com/en-us/download/details.aspx?id=44266
	Nếu xài python 3 trở lên thì cài Microsoft Visual C++ Build Tools, link download: https://visualstudio.microsoft.com/thank-you-downloading-visual-studio/?sku=BuildTools&rel=15)
	+ Chạy project bằng lệnh: python app.py

* URI để gọi các api

