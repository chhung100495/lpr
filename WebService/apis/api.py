from flask import Blueprint, request, send_from_directory
from flask_jsonpify import jsonify
from ConfigParser import SafeConfigParser
import os, sys

lib_path = os.path.abspath(os.path.join('fn'))
sys.path.insert(0, lib_path)
lib_path = os.path.abspath(os.path.join('machine_learning/ml_codes'))
sys.path.insert(1, lib_path)

# import
import LPR
import k_neighbors
import svm

lpr = Blueprint('lpr', __name__,)

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])
def allowed_file(filename):
    return '.' in filename and \
        filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@lpr.route('/', methods=['POST'])
def processing():
    try:
        # check file exists or not
        if 'image' not in request.files:
            return jsonify({"message": "No file part"}), 500
        files = request.files['image']
        # submit an empty part without filename
        if files.filename == '':
            return jsonify({"message": "No selected file"}), 500
        if files and allowed_file(files.filename):
            # save file
            image_name = 'source_image.' + files.filename.rsplit('.', 1)[1].lower()
            files.save(os.path.join('process_images', image_name))

            LPR.image_path = os.path.abspath(os.path.join('process_images/' + image_name))
            result = LPR.execute()

            # gets the main app root directory to get file config path
            config_path = os.path.abspath(os.path.join('config.ini'))
            # read the percentage of correct predictions
            config = SafeConfigParser()
            config.read(config_path)
            percentage = config.get('model', 'percentage')
            response = {
                "plate_text": result,
                "plate_objects": "http://localhost:5000/lpr/processImages/plate_objects.jpg",
                "segment_characters": "http://localhost:5000/lpr/processImages/segment_characters.jpg",
                "percentage": percentage
            }
            return jsonify(response), 200
    except Exception as e:
        print("Error: " + str(e))
        return jsonify({"message": "False"}), 500

@lpr.route('/processImages/<fileName>', methods=['GET'])
def sendImage(fileName):
    try:
        return send_from_directory('process_images', fileName)
    except Exception as e:
        print("Error: " + str(e))
        return jsonify({"message": "False"}), 500

@lpr.route('/buildModel/<classifier>', methods=['GET'])
def buildModel(classifier):
    try:
        if classifier == 'SVM':
            svm.execute()
            return jsonify({"message": "Success"}), 200
        if classifier == 'KNN':
            k_neighbors.execute()
            return jsonify({"message": "Success"}), 200
    except Exception as e:
        print("Error: " + str(e))
        return jsonify({"message": "False"}), 500