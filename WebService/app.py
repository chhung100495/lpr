from flask import Flask
from flask_cors import CORS
import os, sys
# get the path to the modules directory in the current project
lib_path = os.path.abspath(os.path.join('apis'))
# add the directory to load into the system
sys.path.insert(0, lib_path)

# import
from api import lpr

# define app using flask
app = Flask(__name__)

CORS(app)

# register routes from urls
# app.register_blueprint(urls_blueprint)
# register routes with specific prefix
app.register_blueprint(lpr, url_prefix='/lpr')

if __name__ == '__main__':
    app.run(host='localhost', port=5000, debug=True)