from sklearn.svm import SVC
from ml_config import MachineLearningConfig
from ml_validation import AccuracyValidation
import os.path

def execute():

    config = MachineLearningConfig("SVC_model", "SVC_model.pkl")

    image_data, target_data = config.read_training_data(config.training_data[0])

    # the kernel can be 'linear', 'poly' or 'rbf'
    # the probability was set to True so as to show
    # how sure the model is of it's prediction
    svc_model = SVC(kernel='linear', probability=True)

    # let's train the model with all the input data
    svc_model.fit(image_data, target_data)

    config.save_model(svc_model, 'SVC_model')

    ###############################################
    # for validation and testing purposes
    ###############################################

    validate = AccuracyValidation()

    validate.split_validation(svc_model, image_data, target_data, True)

    validate.cross_validation(svc_model, 3, image_data,
        target_data)

    ###############################################
    # end of validation and testing
    ###############################################