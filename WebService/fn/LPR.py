import os, sys
import plotting

from ConfigParser import SafeConfigParser
from preprocess import PreProcess
from ocr import OCROnObjects
from textclassification import TextClassification
from flask_jsonpify import jsonify

lib_path = os.path.abspath(os.path.join('machine_learning'))
sys.path.insert(0, lib_path)

# import
from deepMachine import DeepMachineLearning

image_path = ''
plate_text = ''

def license_plate_extract(plate_like_objects, pre_process):
    number_of_candidates = len(plate_like_objects)

    if number_of_candidates == 0:
        print("Message: License plate could not be located")
        return []

    if number_of_candidates == 1:
        license_plate = pre_process.inverted_threshold(plate_like_objects[0])
    else:
        license_plate = pre_process.validate_plate(plate_like_objects)

    return license_plate

def execute():
    """
    runs the full license plate recognition process.
    """
    models_folder = os.path.abspath(os.path.join('machine_learning/ml_models'))

    pre_process = PreProcess(image_path)

    plate_like_objects = pre_process.get_plate_like_objects()
    plotting.plot_cca(pre_process.full_car_image,
        pre_process.plate_objects_cordinates, "plate_objects")

    license_plate = license_plate_extract(plate_like_objects, pre_process)

    if len(license_plate) == 0:
        return False

    ocr_instance = OCROnObjects(license_plate)

    if ocr_instance.candidates == {}:
        print("Message: No character was segmented")
        return False

    plotting.plot_cca(license_plate, ocr_instance.candidates['coordinates'], "segment_characters")

    deep_learn = DeepMachineLearning()

    config = SafeConfigParser()
    config.read('config.ini')
    folder = config.get('model', 'folder')
    filename = config.get('model', 'filename')

    text_result = deep_learn.learn(ocr_instance.candidates['fullscale'],
        os.path.join(models_folder, folder, filename),
        (20, 20))

    text_phase = TextClassification()
    scattered_plate_text = text_phase.get_text(text_result)
    plate_text = text_phase.text_reconstruction(scattered_plate_text,
        ocr_instance.candidates['columnsVal'])
    return plate_text