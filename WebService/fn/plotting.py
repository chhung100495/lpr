import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

def plot_cca(image, objects_cordinates, filename):
    fig, ax = plt.subplots(ncols=1, nrows=1, figsize=(12, 12))
    ax.imshow(image, cmap='gray')

    for each_cordinate in objects_cordinates:
        min_row, min_col, max_row, max_col = each_cordinate
        region_height = max_row - min_row
        region_width = max_col - min_col
        bound_box = mpatches.Rectangle((min_col, min_row), region_width,
            region_height, fill=False, edgecolor='red', linewidth=2)
        ax.add_patch(bound_box)

    plt.savefig("process_images/"+ filename + ".jpg")
    plt.close()