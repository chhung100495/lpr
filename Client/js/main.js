var file;
let successNotification;
let errorNotification;
let warningNotification;

$(document).ready(function(e) {
  $(".loadImage").click(function(){
			$("#selectFile").trigger('click');
  });

  var srcImageSegmentCharacters = $('#imageSegmentCharacters').attr('src');
  var srcImagePlateObjects = $('#imagePlateObjects').attr('src');
  if(srcImageSegmentCharacters == "#" && srcImagePlateObjects == "#") {
    $('#imagePlateObjects').hide();
    $('#imageSegmentCharacters').hide();
  }

  successNotification = window.createNotification({
    // close on click
    closeOnClick: true,

    // displays close button
    displayCloseButton: true,

    // nfc-top-left
    // nfc-bottom-right
    // nfc-bottom-left
    positionClass: 'nfc-top-right',

    // callback
    onclick: false,

    // timeout in milliseconds
    showDuration: 3500,

    // success, info, warning, error, and none
    theme: 'success'
  });
  errorNotification = window.createNotification({
    closeOnClick: true,
    displayCloseButton: true,
    positionClass: 'nfc-top-right',
    onclick: false,
    showDuration: 3500,
    theme: 'error'
  });
  warningNotification = window.createNotification({
    closeOnClick: true,
    displayCloseButton: true,
    positionClass: 'nfc-top-right',
    onclick: false,
    showDuration: 3500,
    theme: 'warning'
  });
});

function initBlockUI() {
  $.blockUI({ css: {
    border: 'none',
    padding: '15px',
    backgroundColor: '#000',
    '-webkit-border-radius': '10px',
    '-moz-border-radius': '10px',
    opacity: .5,
    color: '#fff'
  }});
}

function readURL(input) {
  if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
          $('#sourceImage')
              .attr('src', e.target.result)
      };
      file = input.files[0];
      reader.readAsDataURL(input.files[0]);
  }

  // reset
  $('#plateText').empty();
  $('#conf').empty();
  $('#conf').append("???");
  $('#sourceImage').attr("src", "./img/image-placeholder.jpg");
  $('#imagePlateObjects').attr("src", "#");
  $('#imageSegmentCharacters').attr("src", "#");
  $('#imagePlateObjects').hide();
  $('#imageSegmentCharacters').hide();
}

$('.processing').click(function() {
  if($("#selectFile").get(0).files.length == 0) {
    warningNotification({
      title: 'Warning',
      message: 'Please choose image to load before',
    });
    return;
  }
  var formData = new FormData();
  formData.append('image', file);
  initBlockUI();
  $.ajax({
    type: 'POST',
    url: 'http://localhost:5000/lpr/',
    data: formData,
    contentType: false,
    processData: false,
    dataType: 'json',
    cache: false,
    timeout: 20000,
  }).done(function(data) {
    $.unblockUI();
    successNotification({
      title: 'Success',
      message: 'Recognising success',
    });
    $('#plateText').empty();
    $('#plateText').append(data.plate_text);
    $('#imagePlateObjects').attr("src", data.plate_objects +"?"+ new Date().getTime());
    $('#imageSegmentCharacters').attr("src", data.segment_characters +"?"+ new Date().getTime());
    $('#imagePlateObjects').show();
    $('#imageSegmentCharacters').show();
    $('#conf').empty();
    $('#conf').append(data.percentage + "%");
  }).fail(function(xhr, status, err) {
    $.unblockUI();
    errorNotification({
      title: 'Error',
      message: err,
    });
    console.log(status);
    console.log(err);
  });
})

$('.machineLearning').click(function() {
  $('#myModal').modal('show');
})

$('.btn-saveChanges').click(function(){
  $('#myModal').modal('hide');
  var classifier = $("input[name='optionsRadios']:checked").val();
  if(classifier){
    initBlockUI();
    $.ajax({
      type: 'GET',
      url: 'http://localhost:5000/lpr/buildModel/' + classifier,
      dataType: 'json',
      timeout: 10000,
    }).done(function(data) {
      $.unblockUI();
      successNotification({
        title: 'Success',
        message: 'Building model success',
      });
    }).fail(function(xhr, status, err) {
      $.unblockUI();
      errorNotification({
        title: 'Error',
        message: err,
      });
      console.log(status);
      console.log(err);
    });
  }
});